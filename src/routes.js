import Login from './components/Login.vue'
import Register from './components/Register.vue'
import Users from './components/Users.vue'

export const routes = [
{ 
	path:'/login',  
	component:Login
},
{ 
	path:'/sign-up',  
	component:Register
},
{ 
	path:'/users',  
	component:Users
}
]